package puppet_test

import (
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"gitlab.oit.duke.edu/oit-ssi-systems/influxdb-input-puppetdebug/puppet"
)

func TestMetricsResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	page, err := ioutil.ReadFile("./test_data/debug.json")
	if err != nil {
		log.Fatal(err)
	}
	//httpmock.RegisterResponder("GET", "http://localhost/status/v1/services?level=debug",
	httpmock.RegisterResponder("GET", `/status/v1/services`,
		httpmock.NewStringResponder(200, string(page)))

	cfg := &puppet.ClientConfig{
		BaseURL: "http://localhost",
	}
	c := puppet.NewClient(cfg, http.DefaultClient)

	mr, err := c.GetMetricResponse()
	if err != nil {
		t.Error(err)
	}
	// Make sure all the services filled in, at least minimally
	assert.Equal(t, mr.JRubyMetrics.DetailLevel, "debug")
	assert.Equal(t, mr.StatusService.DetailLevel, "debug")
	assert.Equal(t, mr.Server.DetailLevel, "debug")
	assert.Equal(t, mr.PuppetProfiler.DetailLevel, "debug")
	assert.Equal(t, mr.Master.DetailLevel, "debug")

}

func TestMetrics(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	page, err := ioutil.ReadFile("./test_data/debug.json")
	if err != nil {
		log.Fatal(err)
	}
	//httpmock.RegisterResponder("GET", "http://localhost/status/v1/services?level=debug",
	httpmock.RegisterResponder("GET", `/status/v1/services`,
		httpmock.NewStringResponder(200, string(page)))

	cfg := &puppet.ClientConfig{
		BaseURL: "http://localhost",
	}
	c := puppet.NewClient(cfg, http.DefaultClient)

	ms, err := c.GetMetrics()
	if err != nil {
		t.Error(err)
	}
	// Make sure all the services filled in, at least minimally
	assert.Greater(t, len(ms), 0)

}
