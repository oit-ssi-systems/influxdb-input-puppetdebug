package puppet

import (
	"fmt"
	"net/http"
	"time"
)

func (c *Client) GetMetricResponse() (*MetricsResponse, error) {
	var mr *MetricsResponse

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/status/v1/services?level=debug", c.BaseURL), nil)
	if err != nil {
		return nil, err
	}
	//root := new(registryRoot)
	//r := &DeviceList{}
	//resp := &Response{}

	_, err = c.sendRequest(req, &mr)
	if err != nil {
		return nil, err
	}
	return mr, nil
}

func (c *Client) GetMetrics() ([]GenericPuppetMetric, error) {
	var ms []GenericPuppetMetric
	mr, err := c.GetMetricResponse()
	if err != nil {
		return nil, err
	}
	now := time.Now()

	// Get 'Server' metrics (I think this is the same as what's in 'Master')
	for _, item := range mr.Server.Status.Experimental.Http_Client_Metrics {
		m := GenericPuppetMetric{
			Service: "server-http",
			Value:   item.Count,
			Labels:  map[string]string{"item": item.Metric_Name},
			Name:    "client",
			Type:    "count",
			Time:    now,
		}
		ms = append(ms, m)
	}

	for _, item := range mr.Server.Status.Experimental.Http_Metrics {
		m := GenericPuppetMetric{
			Service: "server-http",
			Value:   item.Count,
			Labels:  map[string]string{"route": item.Route_Id},
			Name:    "route",
			Type:    "count",
			Time:    now,
		}
		ms = append(ms, m)
	}

	// Puppet Profiler now
	for _, item := range mr.PuppetProfiler.Status.Experimental.Catalog_Metrics {
		m := GenericPuppetMetric{
			Service: "puppet-profiler",
			Value:   item.Count,
			Labels:  map[string]string{"catalog": item.Metric},
			Name:    "catalog",
			Type:    "count",
			Time:    now,
		}
		ms = append(ms, m)
	}

	for _, item := range mr.PuppetProfiler.Status.Experimental.Function_Metrics {
		m := GenericPuppetMetric{
			Service: "puppet-profiler",
			Value:   item.Count,
			Labels:  map[string]string{"function": item.Function},
			Name:    "function",
			Type:    "count",
			Time:    now,
		}
		ms = append(ms, m)
	}

	for _, item := range mr.PuppetProfiler.Status.Experimental.Puppetdb_Metrics {
		m := GenericPuppetMetric{
			Service: "puppet-profiler",
			Value:   item.Count,
			//Name:    item.Metric,
			Labels: map[string]string{"puppetdb": item.Metric},
			Name:   "puppetdb",
			Type:   "count",
			Time:   now,
		}
		ms = append(ms, m)
	}
	for _, item := range mr.PuppetProfiler.Status.Experimental.Resource_Metrics {
		m := GenericPuppetMetric{
			Service: "puppet-profiler",
			Value:   item.Count,
			//Name:    item.Resource,
			Labels: map[string]string{"resource": item.Resource},
			Name:   "resource",
			Type:   "count",
			Time:   now,
		}
		ms = append(ms, m)
	}

	// Now some of the more specific stuff
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "cpu-usage",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Cpu_Usage,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "gc-cpu-usage",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Gc_Cpu_Usage,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "uptime",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Up_Time_Ms,
		Type:    "ms",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "threads",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Threading.Thread_Count,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "heap_used",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Heap_Memory.Used,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "heap_committed",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Heap_Memory.Committed,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "non_heap_used",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Non_Heap_Memory.Used,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "non_heap_committed",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Non_Heap_Memory.Committed,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "file_descriptors_used",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.File_Descriptors.Used,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "file_descriptors_max",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.File_Descriptors.Max,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Metaspace"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Metaspace.Usage.Committed,
		Type:    "committed",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Metaspace"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Metaspace.Usage.Used,
		Type:    "used",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Old_Gen"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Old_Gen.Usage.Committed,
		Type:    "committed",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Old_Gen"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Old_Gen.Usage.Used,
		Type:    "used",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Compressed_Class_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Compressed_Class_Space.Usage.Committed,
		Type:    "committed",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Compressed_Class_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Compressed_Class_Space.Usage.Used,
		Type:    "used",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Survivor_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Survivor_Space.Usage.Committed,
		Type:    "committed",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Survivor_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Survivor_Space.Usage.Used,
		Type:    "used",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Eden_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Eden_Space.Usage.Committed,
		Type:    "committed",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "heap", "pool": "PS_Eden_Space"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.PS_Eden_Space.Usage.Used,
		Type:    "used",
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Code_Cache"},
		Type:    "committed",
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Code_Cache.Usage.Committed,
		Time:    now,
	})
	ms = append(ms, GenericPuppetMetric{
		Service: "status",
		Name:    "memory-pools",
		Labels:  map[string]string{"type": "non_heap", "pool": "Code_Cache"},
		Value:   mr.StatusService.Status.Experimental.Jvm_Metrics.Memory_Pools.Code_Cache.Usage.Used,
		Type:    "used",
		Time:    now,
	})

	return ms, nil
}
