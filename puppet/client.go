package puppet

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	log "github.com/sirupsen/logrus"
	"moul.io/http2curl"
)

type ClientConfig struct {
	InsecureSkipVerify bool
	BaseURL            string
}

type Client struct {
	// HTTP client used to communicate with the DO API.
	client *http.Client

	// Base URL for API requests.
	BaseURL            *url.URL
	InsecureSkipVerify bool
}

func NewClient(c *ClientConfig, httpClient *http.Client) *Client {
	if httpClient == nil {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: c.InsecureSkipVerify},
		}
		httpClient = http.DefaultClient
		httpClient.Transport = tr
	}

	baseURL, _ := url.Parse(c.BaseURL)
	client := &Client{client: httpClient, BaseURL: baseURL}

	return client
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*http.Response, error) {
	//req.Header.Set("Content-Type", "application/json; charset=utf-8")
	//req.Header.Set("Accept", "application/json; charset=utf-8")
	//req.Header.Set("User-Agent", userAgent)
	//req.SetBasicAuth(c.Username, c.Token)

	command, _ := http2curl.GetCurlCommand(req)
	log.Debugln(command)

	res, err := c.client.Do(req)
	if err != nil {
		log.Warn("Got an error on Do")
		log.Warn(err)
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return nil, fmt.Errorf("Too many requests.  Check rate limit and make sure the userAgent is set right")
		} else {
			return nil, fmt.Errorf("unknown error, status code: %d", res.StatusCode)
		}
	}

	if err = json.NewDecoder(res.Body).Decode(&v); err != nil {
		return nil, err
	}

	return res, nil

}

type errorResponse struct {
	Status    string `json:"status"`
	ErrorType string `json:"errorType"`
	Error     string `json:"error"`
	Message   string `json:"message,omitempty"`
}

type Response struct {
	*http.Response
}
