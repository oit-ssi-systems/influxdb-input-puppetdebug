package puppet

import (
	"fmt"
	"strings"
	"time"
)

// Generic Puppet Metric that will be returned to Influx
type GenericPuppetMetric struct {
	Service string
	Value   float64
	Name    string
	Type    string
	Labels  map[string]string
	Time    time.Time
}

func (m *GenericPuppetMetric) InfluxFormat() []byte {
	var timeStamp int64
	if m.Time.IsZero() {
		now := time.Now()
		timeStamp = now.Unix()

	} else {
		timeStamp = m.Time.Unix()

	}

	tags := make(map[string]string)
	var tagTmp []string
	tags["service"] = m.Service
	for k, v := range m.Labels {
		tags[k] = v
	}
	for k, v := range tags {
		tagTmp = append(tagTmp, fmt.Sprintf("%v=%v", k, v))
	}
	tagString := strings.Join(tagTmp[:], ",")
	fieldName := m.Name
	if m.Type != "" {
		fieldName = fmt.Sprintf("%v_%v", fieldName, m.Type)
	}
	// Clean up field name
	fieldName = strings.ReplaceAll(fieldName, "/*/", "all")
	fieldName = strings.ReplaceAll(fieldName, "/", "_")
	fieldName = strings.ReplaceAll(fieldName, "-", "_")
	fieldName = strings.ReplaceAll(fieldName, ".", "_")
	fieldName = strings.ReplaceAll(fieldName, ":", "_")
	fieldName = strings.ReplaceAll(fieldName, "[", "__")
	fieldName = strings.ReplaceAll(fieldName, "]", "__")
	fieldName = strings.ToLower(fieldName)

	retString := fmt.Sprintf("puppet_debug,%v %v=%f %v", tagString, fieldName, m.Value, timeStamp)

	return []byte(retString)

}

type Master struct {
	DetailLevel          string  `json:"detail_level,omitempty"`
	ServiceStatusVersion float64 `json:"service_status_version,omitempty"`
	ServiceVersion       string  `json:"service_version,omitempty"`
	State                string  `json:"state,omitempty"`
	Status               struct {
		Experimental struct {
			Http_Client_Metrics []struct {
				Aggregate   float64  `json:"aggregate,omitempty"`
				Count       float64  `json:"count,omitempty"`
				Mean        float64  `json:"mean,omitempty"`
				Metric_Id   []string `json:"metric-id,omitempty"`
				Metric_Name string   `json:"metric-name,omitempty"`
			} `json:"http-client-metrics,omitempty"`
			Http_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
				Route_Id  string  `json:"route-id,omitempty"`
			} `json:"http-metrics,omitempty"`
		} `json:"experimental,omitempty"`
	} `json:"status,omitempty"`
}

type PuppetProfiler struct {
	DetailLevel          string  `json:"detail_level,omitempty"`
	ServiceStatusVersion float64 `json:"service_status_version,omitempty"`
	ServiceVersion       string  `json:"service_version,omitempty"`
	State                string  `json:"state,omitempty"`
	Status               struct {
		Experimental struct {
			Catalog_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
				Metric    string  `json:"metric,omitempty"`
			} `json:"catalog-metrics,omitempty"`
			Function_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Function  string  `json:"function,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
			} `json:"function-metrics,omitempty"`
			Inline_Metrics   []interface{} `json:"inline-metrics,omitempty"`
			Puppetdb_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
				Metric    string  `json:"metric,omitempty"`
			} `json:"puppetdb-metrics,omitempty"`
			Resource_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
				Resource  string  `json:"resource,omitempty"`
			} `json:"resource-metrics,omitempty"`
		} `json:"experimental,omitempty"`
	} `json:"status,omitempty"`
}

type Server struct {
	DetailLevel          string  `json:"detail_level,omitempty"`
	ServiceStatusVersion float64 `json:"service_status_version,omitempty"`
	ServiceVersion       string  `json:"service_version,omitempty"`
	State                string  `json:"state,omitempty"`
	Status               struct {
		Experimental struct {
			Http_Client_Metrics []struct {
				Aggregate   float64  `json:"aggregate,omitempty"`
				Count       float64  `json:"count,omitempty"`
				Mean        float64  `json:"mean,omitempty"`
				Metric_Id   []string `json:"metric-id,omitempty"`
				Metric_Name string   `json:"metric-name,omitempty"`
			} `json:"http-client-metrics,omitempty"`
			Http_Metrics []struct {
				Aggregate float64 `json:"aggregate,omitempty"`
				Count     float64 `json:"count,omitempty"`
				Mean      float64 `json:"mean,omitempty"`
				Route_Id  string  `json:"route-id,omitempty"`
			} `json:"http-metrics,omitempty"`
		} `json:"experimental,omitempty"`
	} `json:"status,omitempty"`
}

type StatusService struct {
	DetailLevel          string  `json:"detail_level,omitempty"`
	ServiceStatusVersion float64 `json:"service_status_version,omitempty"`
	ServiceVersion       string  `json:"service_version,omitempty"`
	State                string  `json:"state,omitempty"`
	Status               struct {
		Experimental struct {
			Jvm_Metrics struct {
				Cpu_Usage        float64 `json:"cpu-usage,omitempty"`
				File_Descriptors struct {
					Max  float64 `json:"max,omitempty"`
					Used float64 `json:"used,omitempty"`
				} `json:"file-descriptors,omitempty"`
				Gc_Cpu_Usage float64 `json:"gc-cpu-usage,omitempty"`
				Gc_Stats     struct {
					PS_MarkSweep struct {
						Count        float64 `json:"count,omitempty"`
						Last_Gc_Info struct {
							Duration_Ms float64 `json:"duration-ms,omitempty"`
						} `json:"last-gc-info,omitempty"`
						Total_Time_Ms float64 `json:"total-time-ms,omitempty"`
					} `json:"PS MarkSweep,omitempty"`
					PS_Scavenge struct {
						Count        float64 `json:"count,omitempty"`
						Last_Gc_Info struct {
							Duration_Ms float64 `json:"duration-ms,omitempty"`
						} `json:"last-gc-info,omitempty"`
						Total_Time_Ms float64 `json:"total-time-ms,omitempty"`
					} `json:"PS Scavenge,omitempty"`
				} `json:"gc-stats,omitempty"`
				Heap_Memory struct {
					Committed float64 `json:"committed,omitempty"`
					Init      float64 `json:"init,omitempty"`
					Max       float64 `json:"max,omitempty"`
					Used      float64 `json:"used,omitempty"`
				} `json:"heap-memory,omitempty"`
				Memory_Pools struct {
					Code_Cache struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					} `json:"Code Cache,omitempty"`
					Compressed_Class_Space struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					} `json:"Compressed Class Space,omitempty"`
					Metaspace struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					}
					PS_Eden_Space struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					} `json:"PS Eden Space,omitempty"`
					PS_Old_Gen struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					} `json:"PS Old Gen,omitempty"`
					PS_Survivor_Space struct {
						Type  string `json:"type,omitempty"`
						Usage struct {
							Committed float64 `json:"committed,omitempty"`
							Init      float64 `json:"init,omitempty"`
							Max       float64 `json:"max,omitempty"`
							Used      float64 `json:"used,omitempty"`
						} `json:"usage,omitempty"`
					} `json:"PS Survivor Space,omitempty"`
				} `json:"memory-pools,omitempty"`
				Nio_Buffer_Pools struct {
					Direct struct {
						Count          float64 `json:"count,omitempty"`
						Memory_Used    float64 `json:"memory-used,omitempty"`
						Total_Capacity float64 `json:"total-capacity,omitempty"`
					} `json:"direct,omitempty"`
					Mapped struct {
						Count          float64 `json:"count,omitempty"`
						Memory_Used    float64 `json:"memory-used,omitempty"`
						Total_Capacity float64 `json:"total-capacity,omitempty"`
					} `json:"mapped,omitempty"`
				} `json:"nio-buffer-pools,omitempty"`
				Non_Heap_Memory struct {
					Committed float64 `json:"committed,omitempty"`
					Init      float64 `json:"init,omitempty"`
					Max       float64 `json:"max,omitempty"`
					Used      float64 `json:"used,omitempty"`
				} `json:"non-heap-memory,omitempty"`
				Start_Time_Ms float64 `json:"start-time-ms,omitempty"`
				Threading     struct {
					Peak_Thread_Count float64 `json:"peak-thread-count,omitempty"`
					Thread_Count      float64 `json:"thread-count,omitempty"`
				} `json:"threading,omitempty"`
				Up_Time_Ms float64 `json:"up-time-ms,omitempty"`
			} `json:"jvm-metrics,omitempty"`
		} `json:"experimental,omitempty"`
	} `json:"status,omitempty"`
}

type JRubyMetrics struct {
	DetailLevel          string  `json:"detail_level,omitempty"`
	ServiceStatusVersion float64 `json:"service_status_version,omitempty"`
	ServiceVersion       string  `json:"service_version,omitempty"`
	State                string  `json:"state,omitempty"`
	Status               struct {
		Experimental struct {
			Jruby_Pool_Lock_Status struct {
				Current_State    string `json:"current-state,omitempty"`
				Last_Change_Time string `json:"last-change-time,omitempty"`
			} `json:"jruby-pool-lock-status,omitempty"`
			Metrics struct {
				Average_Borrow_Time       float64 `json:"average-borrow-time,omitempty"`
				Average_Free_Jrubies      float64 `json:"average-free-jrubies,omitempty"`
				Average_Lock_Held_Time    float64 `json:"average-lock-held-time,omitempty"`
				Average_Lock_Wait_Time    float64 `json:"average-lock-wait-time,omitempty"`
				Average_Requested_Jrubies float64 `json:"average-requested-jrubies,omitempty"`
				Average_Wait_Time         float64 `json:"average-wait-time,omitempty"`
				Borrow_Count              float64 `json:"borrow-count,omitempty"`
				Borrow_Retry_Count        float64 `json:"borrow-retry-count,omitempty"`
				Borrow_Timeout_Count      float64 `json:"borrow-timeout-count,omitempty"`
				Borrow_Timers             struct {
					Puppet_V3_Catalog struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-catalog,omitempty"`
					Puppet_V3_FileContent struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-file_content,omitempty"`
					Puppet_V3_FileMetadata struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-file_metadata,omitempty"`
					Puppet_V3_FileMetadatas struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-file_metadatas,omitempty"`
					Puppet_V3_Node struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-node,omitempty"`
					Puppet_V3_Report struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"puppet-v3-report,omitempty"`
					Total struct {
						Count float64 `json:"count,omitempty"`
						Max   float64 `json:"max,omitempty"`
						Mean  float64 `json:"mean,omitempty"`
						Rate  float64 `json:"rate,omitempty"`
					} `json:"total,omitempty"`
				} `json:"borrow-timers,omitempty"`
				Borrowed_Instances []struct {
					Duration_Millis float64 `json:"duration-millis,omitempty"`
					Reason          struct {
						Request struct {
							Request_Method string `json:"request-method,omitempty"`
							Route_Id       string `json:"route-id,omitempty"`
							Uri            string `json:"uri,omitempty"`
						} `json:"request,omitempty"`
					} `json:"reason,omitempty"`
					Time float64 `json:"time,omitempty"`
				} `json:"borrowed-instances,omitempty"`
				Num_Free_Jrubies      float64       `json:"num-free-jrubies,omitempty"`
				Num_Jrubies           float64       `json:"num-jrubies,omitempty"`
				Num_Pool_Locks        float64       `json:"num-pool-locks,omitempty"`
				Queue_Limit_Hit_Count float64       `json:"queue-limit-hit-count,omitempty"`
				Queue_Limit_Hit_Rate  float64       `json:"queue-limit-hit-rate,omitempty"`
				Requested_Count       float64       `json:"requested-count,omitempty"`
				Requested_Instances   []interface{} `json:"requested-instances,omitempty"`
				Return_Count          float64       `json:"return-count,omitempty"`
			} `json:"metrics,omitempty"`
		} `json:"experimental,omitempty"`
	} `json:"status,omitempty"`
}

type MetricsResponse struct {
	JRubyMetrics   JRubyMetrics   `json:"jruby-metrics,omitempty"`
	StatusService  StatusService  `json:"status-service,omitempty"`
	Server         Server         `json:"server,omitempty"`
	PuppetProfiler PuppetProfiler `json:"puppet-profiler,omitempty"`
	Master         Master         `json:"master,omitempty"`
}
