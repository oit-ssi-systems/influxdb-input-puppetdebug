package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/influxdb-input-puppetdebug/puppet"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check PUPPET_URL",
	Short: "Check metrics",
	Long:  `Check metrics. PUPPET_URL should be something like 'https://puppet.foo.com:8140'`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		skipVerify, _ := cmd.Flags().GetBool("skip-verify")
		config := &puppet.ClientConfig{
			InsecureSkipVerify: skipVerify,
			BaseURL:            args[0],
		}
		c := puppet.NewClient(config, nil)
		metrics, err := c.GetMetrics()
		CheckErr(err)
		for _, item := range metrics {
			fmt.Printf("%+v\n", string(item.InfluxFormat()))
		}
	},
}

func init() {
	rootCmd.AddCommand(checkCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// checkCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	checkCmd.Flags().BoolP("skip-verify", "k", false, "Skip TLS Verifications")
}
